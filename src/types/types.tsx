// STYLES

import { StyleProp, ViewStyle, TextStyle, ColorValue } from "react-native";

// TypeScript Type: Container Style
export interface ContainerStyle {
  containerLight: StyleProp<ViewStyle>;
  containerDark: StyleProp<ViewStyle>;
}

// TypeScript Type: Label Text Style
export interface LabelTextStyle {
  labelTextLight: StyleProp<TextStyle>;
  labelTextDark: StyleProp<TextStyle>;
}

// TypeScript Type: Field Text Style
export interface FieldTextStyle {
  fieldTextLight: StyleProp<TextStyle>;
  fieldTextDark: StyleProp<TextStyle>;
}

// TypeScript Type: Modal Header Container Style
export interface ModalHeaderContainerStyle {
  modalHeaderContainerLight: StyleProp<ViewStyle>;
  modalHeaderContainerDark: StyleProp<ViewStyle>;
}

// TypeScript Type: Cancel Text Style
export interface CancelTextStyle {
  cancelTextLight: StyleProp<TextStyle>;
  cancelTextDark: StyleProp<TextStyle>;
}

// TypeScript Type: Done Text Style
export interface DoneTextStyle {
  doneTextLight: ColorValue;
  doneTextDark: ColorValue;
}

// TypeScript Type: Modal Content Container Style
export interface ModalContentContainerStyle {
  modalContentContainerLight: StyleProp<ViewStyle>;
  modalContentContainerDark: StyleProp<ViewStyle>;
}

// TypeScript Type: Picker Item Text Style
export interface PickerItemTextStyle {
  pickerItemTextLight: StyleProp<TextStyle>;
  pickerItemTextDark: StyleProp<TextStyle>;
}

// TypeScript Type: Divider Style
export interface DividerStyle {
  dividerLight: StyleProp<ViewStyle>;
  dividerDark: StyleProp<ViewStyle>;
}

// TypeScript Type: Title Text Style
export interface TitleTextStyle {
  titleTextLight: StyleProp<TextStyle>;
  titleTextDark: StyleProp<TextStyle>;
}

// PICKER
// TypeScript Types: Picker Item
export interface PickerItem {
  label: string;
  value: string;
}
