// Imports: Dependencies
import * as React from 'react';
import { useState, useEffect } from 'react';
import { Button, Dimensions, Platform, StyleSheet, Text, View, TouchableOpacity, ColorValue } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import Modal from 'react-native-modal';

// Imports: TypeScript Types
import { ContainerStyle, LabelTextStyle, FieldTextStyle, CancelTextStyle, DoneTextStyle, ModalHeaderContainerStyle, ModalContentContainerStyle, PickerItemTextStyle, PickerItem } from '../../types/types';

// TypeScript Types: Props
interface Props {
  onChange: (value: string) => void,
  title?: string,
  cancelText?: string,
  doneText?: string,
  defaultValue?: string,
  darkMode?: boolean,
  customStyleContainer?: ContainerStyle,
  customStyleLabelText?: LabelTextStyle,
  customStyleFieldText?: FieldTextStyle,
  customStyleModalHeaderContainer?: ModalHeaderContainerStyle,
  customStyleCancelText?: CancelTextStyle,
  customStyleDoneText?: DoneTextStyle,
  customStyleModalContentContainer?: ModalContentContainerStyle,
  customStylePickerItemText?: PickerItemTextStyle,
};

// Screen Dimensions
const { height, width } = Dimensions.get('window');

// Component: Dropdown (United States)
const DropdownUnitedStates: React.FC<Props> = (props): JSX.Element => {
  // React Hooks: State
  const [ modalVisible, toggle ] = useState<boolean>(false);
  const [ tempValue, setTempValue ] = useState<string>('');
  const [ value, setValue ] = useState<string>('');

  // React Hooks: Lifecycle Method
  useEffect(() => {
    // Check If Default Value Exists
    if (props.defaultValue) {
      setValue(props.defaultValue);
    }
    else {
      // Set State
      setValue('Select');
    }
  }, [props.defaultValue]);

  // United States
  const unitedStates: Array<PickerItem> = [
    { label: 'AL', value: 'AL' },
    { label: 'AK', value: 'AK' },
    { label: 'AZ', value: 'AZ' },
    { label: 'AR', value: 'AR' },
    { label: 'CA', value: 'CA' },
    { label: 'CO', value: 'CO' },
    { label: 'CT', value: 'CT' },
    { label: 'DE', value: 'DE' },
    { label: 'FL', value: 'FL' },
    { label: 'GA', value: 'GA' },
    { label: 'HI', value: 'HI' },
    { label: 'ID', value: 'ID' },
    { label: 'IL', value: 'IL' },
    { label: 'IN', value: 'IN' },
    { label: 'IA', value: 'IA' },
    { label: 'KS', value: 'KS' },
    { label: 'KY', value: 'KY' },
    { label: 'LA', value: 'LA' },
    { label: 'ME', value: 'ME' },
    { label: 'MD', value: 'MD' },
    { label: 'MA', value: 'MA' },
    { label: 'MI', value: 'MI' },
    { label: 'MN', value: 'MN' },
    { label: 'MS', value: 'MS' },
    { label: 'MO', value: 'MO' },
    { label: 'MT', value: 'MT' },
    { label: 'NE', value: 'NE' },
    { label: 'NV', value: 'NV' },
    { label: 'NH', value: 'NH' },
    { label: 'NJ', value: 'NJ' },
    { label: 'NM', value: 'NM' },
    { label: 'NY', value: 'NY' },
    { label: 'NC', value: 'NC' },
    { label: 'ND', value: 'ND' },
    { label: 'OH', value: 'OH' },
    { label: 'OK', value: 'OK' },
    { label: 'OR', value: 'OR' },
    { label: 'PA', value: 'PA' },
    { label: 'RI', value: 'RI' },
    { label: 'SC', value: 'SC' },
    { label: 'SD', value: 'SD' },
    { label: 'TN', value: 'TN' },
    { label: 'TX', value: 'TX' },
    { label: 'UT', value: 'UT' },
    { label: 'VT', value: 'VT' },
    { label: 'VA', value: 'VA' },
    { label: 'WA', value: 'WA' },
    { label: 'WV', value: 'WV' },
    { label: 'WI', value: 'WI' },
    { label: 'WY', value: 'WY' },
  ];

  // Render Container Style
  const renderContainerStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return (
        [{
          display: 'flex',
          width: width - 32,
          marginLeft: 16,
          paddingRight: 16,
          paddingBottom: 12,
          marginBottom: 12,
          borderColor: '#8D8D93',
          borderBottomWidth: StyleSheet.hairlineWidth,
          backgroundColor: undefined,
        }, props.customStyleContainer?.containerDark]
      );
    }
    // Light Mode
    else {
      return (
        [{
          display: 'flex',
          width: width - 32,
          marginLeft: 16,
          paddingRight: 16,
          paddingBottom: 12,
          marginBottom: 12,
          borderColor: '#8A8A8E',
          borderBottomWidth: StyleSheet.hairlineWidth,
          backgroundColor: undefined,
        }, props.customStyleContainer?.containerLight]
      );
    }
  };

  // Render Label Text Style
  const renderLabelTextStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return (
        [{
          fontFamily: 'System',
          fontSize: 11,
          fontWeight: '600',
          textTransform: 'uppercase',
          color: '#8D8D93',
          marginBottom: 7,
        }, props.customStyleLabelText?.labelTextDark]
      );
    }
    // Light Mode
    else {
      return (
        [{
          fontFamily: 'System',
          fontSize: 11,
          fontWeight: '600',
          textTransform: 'uppercase',
          color: '#8A8A8E',
          marginBottom: 7,
        }, props.customStyleLabelText?.labelTextLight]
      );
    }
  };

  // Render Field Text Style
  const renderFieldTextStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return (
        [{
          fontFamily: 'System',
          fontSize: 17,
          fontWeight: '400',
          color: '#FFFFFF',
          alignSelf: 'center',
        }, props.customStyleFieldText?.fieldTextDark]
      );
    }
    // Light Mode
    else {
      return (
        [{
          fontFamily: 'System',
          fontSize: 17,
          fontWeight: '400',
          color: '#000000',
          alignSelf: 'center',
        }, props.customStyleFieldText?.fieldTextLight]
      );
    }
  };

  // Render Modal Header Container Style
  const renderModalHeaderContainerStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return (
        [{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: width,
          height: 45,
          backgroundColor: '#383838',
          borderColor: '#E9E9EB',
          borderBottomWidth: StyleSheet.hairlineWidth,
        }, props.customStyleModalHeaderContainer?.modalHeaderContainerDark]
      );
    }
    // Light Mode
    else {
      return (
        [{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: width,
          height: 45,
          backgroundColor: '#FFFFFF',
          borderColor: '#CED4DA',
          borderBottomWidth: StyleSheet.hairlineWidth,
        }, props.customStyleModalHeaderContainer?.modalHeaderContainerLight]
      );
    }
  };

  // Render Cancel Text Style
  const renderCancelTextStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return (
        [{
          marginLeft: 16,
          fontFamily: 'System',
          color: '#0884FE',
          fontWeight: '400',
          fontSize: 17,
        }, props.customStyleCancelText?.cancelTextDark]
      );
    }
    // Light Mode
    else {
      return (
        [{
          marginLeft: 16,
          fontFamily: 'System',
          color: '#007AFF',
          fontWeight: '400',
          fontSize: 17,
        }, props.customStyleCancelText?.cancelTextLight]
      );
    }
  };

  // Render Done Text Style
  const renderDoneTextStyle = (): ColorValue => {
    // Dark Mode
    if (props.darkMode) {
      return props.customStyleDoneText?.doneTextDark || '#0884FE';
    }
    // Light Mode
    else {
      return props.customStyleDoneText?.doneTextLight || '#007AFF';
    }
  };

  // Render Modal Content Container Style
  const renderModalContentContainerStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return (
        [{
          width: width,
          height: 250,
          backgroundColor: '#121312',
        }, props.customStyleModalHeaderContainer?.modalHeaderContainerDark]
      );
    }
    // Light Mode
    else {
      return (
        [{
          width: width,
          height: 250,
          backgroundColor: '#FFFFFF',
        }, props.customStyleModalHeaderContainer?.modalHeaderContainerLight]
      );
    }
  };

  // Render Picker Item Text Style
  const renderPickerItemStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return (
        {
          color: '#FFFFFF',
        }
      );
    }
    // Light Mode
    else {
      return (
        {
          color: '#000000',
        }
      );
    }
  };

  // Toggle Modal
  const toggleModal = (): void => {
    // Platform: iOS
    if (Platform.OS === 'ios') {
      // React Hook: Toggle Modal
      toggle((modalVisible: boolean) => !modalVisible);
    }
  };

  // Press Cancel
  const pressCancel = (): void => {
    // Set State
    setTempValue(value);

    // Toggle Modal
    toggleModal();
  };

  // Press Done
  const pressDone = (): void => {
    // Set State
    setValue(tempValue);

    // Props: onChange
    props.onChange(tempValue);

    // Toggle Modal
    toggleModal();
  };

  // Select Value
  const selectValue = (value: string) => {
    // Platform: iOS
    if (Platform.OS === 'ios') {
      // Set State
      setTempValue(value);
    }
    // Platform: Android
    else if (Platform.OS === 'android') {
      // Set State
      setValue(value);

      // Props: onChange
      props.onChange(value);
    }
  };

  // Render Picker
  const renderPicker = (): JSX.Element | undefined => {
    // Platform: iOS:
    if (Platform.OS === 'ios') {
      return (
        <View style={renderContainerStyle()}>
          <View style={styles.labelContainer}>
            <Text style={renderLabelTextStyle()}>{props.title === undefined ? 'List' : props.title}</Text>
          </View>

          <TouchableOpacity onPress={() => toggleModal()} style={styles.fieldTextContainer}>
            <Text style={renderFieldTextStyle()} numberOfLines={1}>{value ? value : 'Select'}</Text>
          </TouchableOpacity>

          <Modal
            isVisible={modalVisible}
            style={styles.modal}
            backdropOpacity={.30}
          >
            <View style={styles.modalContainer}>
              <View style={renderModalHeaderContainerStyle()}>
                <TouchableOpacity onPress={() => pressCancel()}>
                    <Text style={renderCancelTextStyle()}>{props.cancelText ? props.cancelText : 'Cancel'}</Text>
                  </TouchableOpacity>

                  <View style={styles.doneButtonContainer}>
                    <Button
                      title={props.doneText ? props.doneText : 'Done'}
                      onPress={() => pressDone()}
                      disabled={value === tempValue ? true : false}
                      color={renderDoneTextStyle()}
                    />
                  </View>
              </View>

              <View style={renderModalContentContainerStyle()}>
                <Picker
                  selectedValue={tempValue !== undefined ? tempValue : value}
                  onValueChange={(value: string) => selectValue(value)}
                >
                  {unitedStates.map((item: PickerItem, i: number) => {
                    return (
                      <Picker.Item
                        key={i}
                        label={item.label}
                        value={item.value}
                        color={renderPickerItemStyle()}
                      />
                    );
                  })}
                </Picker>
              </View>
            </View>
          </Modal>
        </View>
      );
    }
    // Platform: Android
    else if (Platform.OS === 'android') {
      return (
        <View style={renderContainerStyle()}>
          <View style={styles.labelContainer}>
            <Text style={renderLabelTextStyle()}>{props.title}</Text>
          </View>

          <Picker
            mode="dropdown"
            selectedValue={value}
            style={{height: 60, width: width - 16}}
            onValueChange={(value: string) => setValue(value)}
          >
            {unitedStates.map((item: PickerItem, i: number) => {
              return (
                <Picker.Item
                  key={i}
                  label={item.label}
                  value={item.value}
                  color={renderPickerItemStyle()}
                />
              );
            })}
          </Picker>
        </View>
      );
    }
  };

  return (
    <>{renderPicker()}</>
  );
};

// Styles
const styles = StyleSheet.create({
  modal: {
    margin: 0,
  },
  modalContainer: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  doneButtonContainer: {
    marginRight: 7,
  },
  labelContainer: {
    width: width - 32,
    marginBottom: 4,
  },
  fieldTextContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

// Exports
export default DropdownUnitedStates;
