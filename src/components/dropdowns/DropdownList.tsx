// Imports: Dependencies
import * as React from "react";
import { useState, useEffect } from "react";
import {
  Button,
  Dimensions,
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ColorValue,
} from "react-native";
import { Picker } from "@react-native-picker/picker";
import Modal from "react-native-modal";

// Imports: TypeScript Types
import {
  ContainerStyle,
  LabelTextStyle,
  FieldTextStyle,
  CancelTextStyle,
  DoneTextStyle,
  ModalHeaderContainerStyle,
  ModalContentContainerStyle,
  PickerItemTextStyle,
  PickerItem,
} from "../../types/types";

// TypeScript Types: Props
interface Props {
  items: Array<PickerItem>;
  onChange: (value: string) => void;
  title?: string;
  cancelText?: string;
  doneText?: string;
  defaultValue?: string;
  darkMode?: boolean;
  customStyleContainer?: ContainerStyle;
  customStyleLabelText?: LabelTextStyle;
  customStyleFieldText?: FieldTextStyle;
  customStyleModalHeaderContainer?: ModalHeaderContainerStyle;
  customStyleCancelText?: CancelTextStyle;
  customStyleDoneText?: DoneTextStyle;
  customStyleModalContentContainer?: ModalContentContainerStyle;
  customStylePickerItemText?: PickerItemTextStyle;
}

// Screen Dimensions
const { height, width } = Dimensions.get("window");

// Component: Dropdown (List)
const DropdownList: React.FC<Props> = (props): JSX.Element => {
  // React Hooks: State
  const [modalVisible, toggle] = useState<boolean>(false);
  const [tempValue, setTempValue] = useState<string>("");
  const [value, setValue] = useState<string>("");

  // React Hooks: Lifecycle Method
  useEffect(() => {
    // Check If Default Value Exists
    if (props.defaultValue) {
      setValue(props.defaultValue);
    } else {
      // Set State
      setValue("Select");
    }
  }, [props.defaultValue]);

  // Render Container Style
  const renderContainerStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return {
        display: "flex",
        width: width - 32,
        marginLeft: 16,
        paddingRight: 16,
        paddingBottom: 12,
        marginBottom: 12,
        borderColor: "#8D8D93",
        borderBottomWidth: StyleSheet.hairlineWidth,
        backgroundColor: undefined,
      };
    }
    // Light Mode
    else {
      return {
        display: "flex",
        width: width - 32,
        marginLeft: 16,
        paddingRight: 16,
        paddingBottom: 12,
        marginBottom: 12,
        borderColor: "#8A8A8E",
        borderBottomWidth: StyleSheet.hairlineWidth,
        backgroundColor: undefined,
      };
    }
  };

  // Render Label Text Style
  const renderLabelTextStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return {
        fontFamily: "System",
        fontSize: 11,
        fontWeight: "600",
        textTransform: "uppercase",
        color: "#8D8D93",
        marginBottom: 7,
      };
    }
    // Light Mode
    else {
      return {
        fontFamily: "System",
        fontSize: 11,
        fontWeight: "600",
        textTransform: "uppercase",
        color: "#8A8A8E",
        marginBottom: 7,
      };
    }
  };

  // Render Field Text Style
  const renderFieldTextStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return {
        fontFamily: "System",
        fontSize: 17,
        fontWeight: "400",
        color: "#FFFFFF",
        alignSelf: "center",
      };
    }
    // Light Mode
    else {
      return {
        fontFamily: "System",
        fontSize: 17,
        fontWeight: "400",
        color: "#000000",
        alignSelf: "center",
      };
    }
  };

  // Render Modal Header Container Style
  const renderModalHeaderContainerStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: width,
        height: 45,
        backgroundColor: "#383838",
        borderColor: "#E9E9EB",
        borderBottomWidth: StyleSheet.hairlineWidth,
      };
    }
    // Light Mode
    else {
      return {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: width,
        height: 45,
        backgroundColor: "#FFFFFF",
        borderColor: "#CED4DA",
        borderBottomWidth: StyleSheet.hairlineWidth,
      };
    }
  };

  // Render Cancel Text Style
  const renderCancelTextStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return {
        marginLeft: 16,
        fontFamily: "System",
        color: "#0884FE",
        fontWeight: "400",
        fontSize: 17,
      };
    }
    // Light Mode
    else {
      return {
        marginLeft: 16,
        fontFamily: "System",
        color: "#007AFF",
        fontWeight: "400",
        fontSize: 17,
      };
    }
  };

  // Render Done Text Style
  const renderDoneTextStyle = (): ColorValue => {
    // Dark Mode
    if (props.darkMode) {
      return props.customStyleDoneText?.doneTextDark || "#0884FE";
    }
    // Light Mode
    else {
      return props.customStyleDoneText?.doneTextLight || "#007AFF";
    }
  };

  // Render Modal Content Container Style
  const renderModalContentContainerStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return {
        width: width,
        height: 250,
        backgroundColor: "#121312",
      };
    }
    // Light Mode
    else {
      return {
        width: width,
        height: 250,
        backgroundColor: "#FFFFFF",
      };
    }
  };

  // Render Picker Item Text Style
  const renderPickerItemStyle = (): any => {
    // Dark Mode
    if (props.darkMode) {
      return {
        color: "#FFFFFF",
      };
    }
    // Light Mode
    else {
      return {
        color: "#000000",
      };
    }
  };

  // Toggle Modal
  const toggleModal = (): void => {
    // Platform: iOS
    if (Platform.OS === "ios") {
      // React Hook: Toggle Modal
      toggle((modalVisible: boolean) => !modalVisible);
    }
  };

  // Press Cancel
  const pressCancel = (): void => {
    // Set State
    setTempValue(value);

    // Toggle Modal
    toggleModal();
  };

  // Press Done
  const pressDone = (): void => {
    // Set State
    setValue(tempValue);

    // Props: onChange
    props.onChange(tempValue);

    // Toggle Modal
    toggleModal();
  };

  // Select Value
  const selectValue = (value: string) => {
    // Platform: iOS
    if (Platform.OS === "ios") {
      // Set State
      setTempValue(value);
    }
    // Platform: Android
    else if (Platform.OS === "android") {
      // Set State
      setValue(value);

      // Props: onChange
      props.onChange(value);
    }
  };

  // Render Picker
  const renderPicker = (): JSX.Element | undefined => {
    // Platform: iOS:
    if (Platform.OS === "ios") {
      return (
        <View
          style={[
            renderContainerStyle(),
            props.darkMode
              ? props.customStyleContainer?.containerDark
              : props.customStyleContainer?.containerLight,
          ]}
        >
          <View style={styles.labelContainer}>
            <Text
              style={[
                renderLabelTextStyle(),
                props.darkMode
                  ? props.customStyleLabelText?.labelTextDark
                  : props.customStyleLabelText?.labelTextLight,
              ]}
            >
              {props.title === undefined ? "List" : props.title}
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => toggleModal()}
            style={styles.fieldTextContainer}
          >
            <Text
              style={[
                renderFieldTextStyle(),
                props.darkMode
                  ? props.customStyleFieldText?.fieldTextDark
                  : props.customStyleFieldText?.fieldTextLight,
              ]}
              numberOfLines={1}
            >
              {value ? value : "Select"}
            </Text>
          </TouchableOpacity>

          <Modal
            isVisible={modalVisible}
            style={styles.modal}
            backdropOpacity={0.3}
          >
            <View style={styles.modalContainer}>
              <View
                style={[
                  renderModalHeaderContainerStyle(),
                  props.darkMode
                    ? props.customStyleModalHeaderContainer
                        ?.modalHeaderContainerDark
                    : props.customStyleModalHeaderContainer
                        ?.modalHeaderContainerLight,
                ]}
              >
                <TouchableOpacity onPress={() => pressCancel()}>
                  <Text
                    style={[
                      renderCancelTextStyle(),
                      props.darkMode
                        ? props.customStyleCancelText?.cancelTextDark
                        : props.customStyleCancelText?.cancelTextLight,
                    ]}
                  >
                    {props.cancelText ? props.cancelText : "Cancel"}
                  </Text>
                </TouchableOpacity>

                <View style={styles.doneButtonContainer}>
                  <Button
                    title={props.doneText ? props.doneText : "Done"}
                    onPress={() => pressDone()}
                    disabled={value === tempValue ? true : false}
                    color={renderDoneTextStyle()}
                  />
                </View>
              </View>

              <View
                style={[
                  renderModalContentContainerStyle(),
                  props.darkMode
                    ? props.customStyleModalContentContainer
                        ?.modalContentContainerDark
                    : props.customStyleModalContentContainer
                        ?.modalContentContainerLight,
                ]}
              >
                <Picker
                  selectedValue={tempValue !== undefined ? tempValue : value}
                  onValueChange={(value: string) => selectValue(value)}
                >
                  {props.items.map((item: PickerItem, i: number) => {
                    return (
                      <Picker.Item
                        key={i}
                        label={item.label}
                        value={item.value}
                        color={renderPickerItemStyle()}
                      />
                    );
                  })}
                </Picker>
              </View>
            </View>
          </Modal>
        </View>
      );
    }
    // Platform: Android
    else if (Platform.OS === "android") {
      return (
        <View
          style={[
            renderContainerStyle(),
            props.darkMode
              ? props.customStyleContainer?.containerDark
              : props.customStyleContainer?.containerLight,
          ]}
        >
          <View style={styles.labelContainer}>
            <Text
              style={[
                renderLabelTextStyle(),
                props.darkMode
                  ? props.customStyleLabelText?.labelTextDark
                  : props.customStyleLabelText?.labelTextLight,
              ]}
            >
              {props.title}
            </Text>
          </View>

          <Picker
            mode="dropdown"
            selectedValue={value}
            style={{ height: 60, width: width - 16 }}
            onValueChange={(value: string) => setValue(value)}
          >
            {props.items.map((item: PickerItem, i: number) => {
              return (
                <Picker.Item
                  key={i}
                  label={item.label}
                  value={item.value}
                  color={renderPickerItemStyle()}
                />
              );
            })}
          </Picker>
        </View>
      );
    }
  };

  return <>{renderPicker()}</>;
};

// Styles
const styles = StyleSheet.create({
  modal: {
    margin: 0,
  },
  modalContainer: {
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  doneButtonContainer: {
    marginRight: 7,
  },
  labelContainer: {
    width: width - 32,
    marginBottom: 4,
  },
  fieldTextContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});

// Exports
export default DropdownList;
