import { StyleProp, ViewStyle, TextStyle, ColorValue } from "react-native";
export interface ContainerStyle {
    containerLight: StyleProp<ViewStyle>;
    containerDark: StyleProp<ViewStyle>;
}
export interface LabelTextStyle {
    labelTextLight: StyleProp<TextStyle>;
    labelTextDark: StyleProp<TextStyle>;
}
export interface FieldTextStyle {
    fieldTextLight: StyleProp<TextStyle>;
    fieldTextDark: StyleProp<TextStyle>;
}
export interface ModalHeaderContainerStyle {
    modalHeaderContainerLight: StyleProp<ViewStyle>;
    modalHeaderContainerDark: StyleProp<ViewStyle>;
}
export interface CancelTextStyle {
    cancelTextLight: StyleProp<TextStyle>;
    cancelTextDark: StyleProp<TextStyle>;
}
export interface DoneTextStyle {
    doneTextLight: ColorValue;
    doneTextDark: ColorValue;
}
export interface ModalContentContainerStyle {
    modalContentContainerLight: StyleProp<ViewStyle>;
    modalContentContainerDark: StyleProp<ViewStyle>;
}
export interface PickerItemTextStyle {
    pickerItemTextLight: StyleProp<TextStyle>;
    pickerItemTextDark: StyleProp<TextStyle>;
}
export interface DividerStyle {
    dividerLight: StyleProp<ViewStyle>;
    dividerDark: StyleProp<ViewStyle>;
}
export interface TitleTextStyle {
    titleTextLight: StyleProp<TextStyle>;
    titleTextDark: StyleProp<TextStyle>;
}
export interface PickerItem {
    label: string;
    value: string;
}
