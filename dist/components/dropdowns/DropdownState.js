"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsx_runtime_1 = require("react/jsx-runtime");
const react_1 = require("react");
const react_native_1 = require("react-native");
const picker_1 = require("@react-native-picker/picker");
const react_native_modal_1 = __importDefault(require("react-native-modal"));
;
// Screen Dimensions
const { height, width } = react_native_1.Dimensions.get('window');
// Component: Dropdown (United States)
const DropdownUnitedStates = (props) => {
    // React Hooks: State
    const [modalVisible, toggle] = react_1.useState(false);
    const [tempValue, setTempValue] = react_1.useState('');
    const [value, setValue] = react_1.useState('');
    // React Hooks: Lifecycle Method
    react_1.useEffect(() => {
        // Check If Default Value Exists
        if (props.defaultValue) {
            setValue(props.defaultValue);
        }
        else {
            // Set State
            setValue('Select');
        }
    }, [props.defaultValue]);
    // United States
    const unitedStates = [
        { label: 'AL', value: 'AL' },
        { label: 'AK', value: 'AK' },
        { label: 'AZ', value: 'AZ' },
        { label: 'AR', value: 'AR' },
        { label: 'CA', value: 'CA' },
        { label: 'CO', value: 'CO' },
        { label: 'CT', value: 'CT' },
        { label: 'DE', value: 'DE' },
        { label: 'FL', value: 'FL' },
        { label: 'GA', value: 'GA' },
        { label: 'HI', value: 'HI' },
        { label: 'ID', value: 'ID' },
        { label: 'IL', value: 'IL' },
        { label: 'IN', value: 'IN' },
        { label: 'IA', value: 'IA' },
        { label: 'KS', value: 'KS' },
        { label: 'KY', value: 'KY' },
        { label: 'LA', value: 'LA' },
        { label: 'ME', value: 'ME' },
        { label: 'MD', value: 'MD' },
        { label: 'MA', value: 'MA' },
        { label: 'MI', value: 'MI' },
        { label: 'MN', value: 'MN' },
        { label: 'MS', value: 'MS' },
        { label: 'MO', value: 'MO' },
        { label: 'MT', value: 'MT' },
        { label: 'NE', value: 'NE' },
        { label: 'NV', value: 'NV' },
        { label: 'NH', value: 'NH' },
        { label: 'NJ', value: 'NJ' },
        { label: 'NM', value: 'NM' },
        { label: 'NY', value: 'NY' },
        { label: 'NC', value: 'NC' },
        { label: 'ND', value: 'ND' },
        { label: 'OH', value: 'OH' },
        { label: 'OK', value: 'OK' },
        { label: 'OR', value: 'OR' },
        { label: 'PA', value: 'PA' },
        { label: 'RI', value: 'RI' },
        { label: 'SC', value: 'SC' },
        { label: 'SD', value: 'SD' },
        { label: 'TN', value: 'TN' },
        { label: 'TX', value: 'TX' },
        { label: 'UT', value: 'UT' },
        { label: 'VT', value: 'VT' },
        { label: 'VA', value: 'VA' },
        { label: 'WA', value: 'WA' },
        { label: 'WV', value: 'WV' },
        { label: 'WI', value: 'WI' },
        { label: 'WY', value: 'WY' },
    ];
    // Render Container Style
    const renderContainerStyle = () => {
        // Dark Mode
        if (props.darkMode) {
            return ([{
                    display: 'flex',
                    width: width - 32,
                    marginLeft: 16,
                    paddingRight: 16,
                    paddingBottom: 12,
                    marginBottom: 12,
                    borderColor: '#8D8D93',
                    borderBottomWidth: react_native_1.StyleSheet.hairlineWidth,
                    backgroundColor: undefined,
                }, props.customStyleContainer?.containerDark]);
        }
        // Light Mode
        else {
            return ([{
                    display: 'flex',
                    width: width - 32,
                    marginLeft: 16,
                    paddingRight: 16,
                    paddingBottom: 12,
                    marginBottom: 12,
                    borderColor: '#8A8A8E',
                    borderBottomWidth: react_native_1.StyleSheet.hairlineWidth,
                    backgroundColor: undefined,
                }, props.customStyleContainer?.containerLight]);
        }
    };
    // Render Label Text Style
    const renderLabelTextStyle = () => {
        // Dark Mode
        if (props.darkMode) {
            return ([{
                    fontFamily: 'System',
                    fontSize: 11,
                    fontWeight: '600',
                    textTransform: 'uppercase',
                    color: '#8D8D93',
                    marginBottom: 7,
                }, props.customStyleLabelText?.labelTextDark]);
        }
        // Light Mode
        else {
            return ([{
                    fontFamily: 'System',
                    fontSize: 11,
                    fontWeight: '600',
                    textTransform: 'uppercase',
                    color: '#8A8A8E',
                    marginBottom: 7,
                }, props.customStyleLabelText?.labelTextLight]);
        }
    };
    // Render Field Text Style
    const renderFieldTextStyle = () => {
        // Dark Mode
        if (props.darkMode) {
            return ([{
                    fontFamily: 'System',
                    fontSize: 17,
                    fontWeight: '400',
                    color: '#FFFFFF',
                    alignSelf: 'center',
                }, props.customStyleFieldText?.fieldTextDark]);
        }
        // Light Mode
        else {
            return ([{
                    fontFamily: 'System',
                    fontSize: 17,
                    fontWeight: '400',
                    color: '#000000',
                    alignSelf: 'center',
                }, props.customStyleFieldText?.fieldTextLight]);
        }
    };
    // Render Modal Header Container Style
    const renderModalHeaderContainerStyle = () => {
        // Dark Mode
        if (props.darkMode) {
            return ([{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    width: width,
                    height: 45,
                    backgroundColor: '#383838',
                    borderColor: '#E9E9EB',
                    borderBottomWidth: react_native_1.StyleSheet.hairlineWidth,
                }, props.customStyleModalHeaderContainer?.modalHeaderContainerDark]);
        }
        // Light Mode
        else {
            return ([{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    width: width,
                    height: 45,
                    backgroundColor: '#FFFFFF',
                    borderColor: '#CED4DA',
                    borderBottomWidth: react_native_1.StyleSheet.hairlineWidth,
                }, props.customStyleModalHeaderContainer?.modalHeaderContainerLight]);
        }
    };
    // Render Cancel Text Style
    const renderCancelTextStyle = () => {
        // Dark Mode
        if (props.darkMode) {
            return ([{
                    marginLeft: 16,
                    fontFamily: 'System',
                    color: '#0884FE',
                    fontWeight: '400',
                    fontSize: 17,
                }, props.customStyleCancelText?.cancelTextDark]);
        }
        // Light Mode
        else {
            return ([{
                    marginLeft: 16,
                    fontFamily: 'System',
                    color: '#007AFF',
                    fontWeight: '400',
                    fontSize: 17,
                }, props.customStyleCancelText?.cancelTextLight]);
        }
    };
    // Render Done Text Style
    const renderDoneTextStyle = () => {
        // Dark Mode
        if (props.darkMode) {
            return props.customStyleDoneText?.doneTextDark || '#0884FE';
        }
        // Light Mode
        else {
            return props.customStyleDoneText?.doneTextLight || '#007AFF';
        }
    };
    // Render Modal Content Container Style
    const renderModalContentContainerStyle = () => {
        // Dark Mode
        if (props.darkMode) {
            return ([{
                    width: width,
                    height: 250,
                    backgroundColor: '#121312',
                }, props.customStyleModalHeaderContainer?.modalHeaderContainerDark]);
        }
        // Light Mode
        else {
            return ([{
                    width: width,
                    height: 250,
                    backgroundColor: '#FFFFFF',
                }, props.customStyleModalHeaderContainer?.modalHeaderContainerLight]);
        }
    };
    // Render Picker Item Text Style
    const renderPickerItemStyle = () => {
        // Dark Mode
        if (props.darkMode) {
            return ({
                color: '#FFFFFF',
            });
        }
        // Light Mode
        else {
            return ({
                color: '#000000',
            });
        }
    };
    // Toggle Modal
    const toggleModal = () => {
        // Platform: iOS
        if (react_native_1.Platform.OS === 'ios') {
            // React Hook: Toggle Modal
            toggle((modalVisible) => !modalVisible);
        }
    };
    // Press Cancel
    const pressCancel = () => {
        // Set State
        setTempValue(value);
        // Toggle Modal
        toggleModal();
    };
    // Press Done
    const pressDone = () => {
        // Set State
        setValue(tempValue);
        // Props: onChange
        props.onChange(tempValue);
        // Toggle Modal
        toggleModal();
    };
    // Select Value
    const selectValue = (value) => {
        // Platform: iOS
        if (react_native_1.Platform.OS === 'ios') {
            // Set State
            setTempValue(value);
        }
        // Platform: Android
        else if (react_native_1.Platform.OS === 'android') {
            // Set State
            setValue(value);
            // Props: onChange
            props.onChange(value);
        }
    };
    // Render Picker
    const renderPicker = () => {
        // Platform: iOS:
        if (react_native_1.Platform.OS === 'ios') {
            return (jsx_runtime_1.jsxs(react_native_1.View, Object.assign({ style: renderContainerStyle() }, { children: [jsx_runtime_1.jsx(react_native_1.View, Object.assign({ style: styles.labelContainer }, { children: jsx_runtime_1.jsx(react_native_1.Text, Object.assign({ style: renderLabelTextStyle() }, { children: props.title === undefined ? 'List' : props.title }), void 0) }), void 0),
                    jsx_runtime_1.jsx(react_native_1.TouchableOpacity, Object.assign({ onPress: () => toggleModal(), style: styles.fieldTextContainer }, { children: jsx_runtime_1.jsx(react_native_1.Text, Object.assign({ style: renderFieldTextStyle(), numberOfLines: 1 }, { children: value ? value : 'Select' }), void 0) }), void 0),
                    jsx_runtime_1.jsx(react_native_modal_1.default, Object.assign({ isVisible: modalVisible, style: styles.modal, backdropOpacity: .30 }, { children: jsx_runtime_1.jsxs(react_native_1.View, Object.assign({ style: styles.modalContainer }, { children: [jsx_runtime_1.jsxs(react_native_1.View, Object.assign({ style: renderModalHeaderContainerStyle() }, { children: [jsx_runtime_1.jsx(react_native_1.TouchableOpacity, Object.assign({ onPress: () => pressCancel() }, { children: jsx_runtime_1.jsx(react_native_1.Text, Object.assign({ style: renderCancelTextStyle() }, { children: props.cancelText ? props.cancelText : 'Cancel' }), void 0) }), void 0),
                                        jsx_runtime_1.jsx(react_native_1.View, Object.assign({ style: styles.doneButtonContainer }, { children: jsx_runtime_1.jsx(react_native_1.Button, { title: props.doneText ? props.doneText : 'Done', onPress: () => pressDone(), disabled: value === tempValue ? true : false, color: renderDoneTextStyle() }, void 0) }), void 0)] }), void 0),
                                jsx_runtime_1.jsx(react_native_1.View, Object.assign({ style: renderModalContentContainerStyle() }, { children: jsx_runtime_1.jsx(picker_1.Picker, Object.assign({ selectedValue: tempValue !== undefined ? tempValue : value, onValueChange: (value) => selectValue(value) }, { children: unitedStates.map((item, i) => {
                                            return (jsx_runtime_1.jsx(picker_1.Picker.Item, { label: item.label, value: item.value, color: renderPickerItemStyle() }, i));
                                        }) }), void 0) }), void 0)] }), void 0) }), void 0)] }), void 0));
        }
        // Platform: Android
        else if (react_native_1.Platform.OS === 'android') {
            return (jsx_runtime_1.jsxs(react_native_1.View, Object.assign({ style: renderContainerStyle() }, { children: [jsx_runtime_1.jsx(react_native_1.View, Object.assign({ style: styles.labelContainer }, { children: jsx_runtime_1.jsx(react_native_1.Text, Object.assign({ style: renderLabelTextStyle() }, { children: props.title }), void 0) }), void 0),
                    jsx_runtime_1.jsx(picker_1.Picker, Object.assign({ mode: "dropdown", selectedValue: value, style: { height: 60, width: width - 16 }, onValueChange: (value) => setValue(value) }, { children: unitedStates.map((item, i) => {
                            return (jsx_runtime_1.jsx(picker_1.Picker.Item, { label: item.label, value: item.value, color: renderPickerItemStyle() }, i));
                        }) }), void 0)] }), void 0));
        }
    };
    return (jsx_runtime_1.jsx(jsx_runtime_1.Fragment, { children: renderPicker() }, void 0));
};
// Styles
const styles = react_native_1.StyleSheet.create({
    modal: {
        margin: 0,
    },
    modalContainer: {
        height: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    doneButtonContainer: {
        marginRight: 7,
    },
    labelContainer: {
        width: width - 32,
        marginBottom: 4,
    },
    fieldTextContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
});
// Exports
exports.default = DropdownUnitedStates;
